<?php

/**
 * @file
 * Handles XML-related stuff for Commerce UPS Address Validation - Street Level module.
 */

/*
 * This is what the XML request needs to look like.

<?xml version=”1.0″?>
<AccessRequest xml:lang=”en-US”>
  <AccessLicenseNumber>ACCESS LICENCE NUMBER</AccessLicenseNumber>
  <UserId>UPS USERNAME</UserId>
  <Password>UPS PASSWORD</Password>
</AccessRequest>
<?xml version=”1.0″?>
<AddressValidationRequest xml:lang=”en-US”>
  <Request>
    <TransactionReference>
      <CustomerContext>Your Test Case Summary Description</CustomerContext>
      <XpciVersion>1.0</XpciVersion>
    </TransactionReference>
    <RequestAction>XAV</RequestAction>
    <RequestOption>3</RequestOption>
  </Request>
  <AddressKeyFormat>
    <AddressLine>AIRWAY ROAD SUITE 7</AddressLine>
    <PoliticalDivision2>SAN DIEGO</PoliticalDivision2>
    <PoliticalDivision1>CA</PoliticalDivision1>
    <PostcodePrimaryLow>92154</PostcodePrimaryLow>
    <CountryCode>US</CountryCode>
  </AddressKeyFormat>
</AddressValidationRequest>

*
*/

/*
 * This builds the XML to submit to UPS for Address Validation.
 */
function commerce_ups_address_validation_build_xml_request($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Determine the shipping profile reference field name for the order.
  $field_name = commerce_physical_order_shipping_field_name($order);
  $shipping_profile = $order_wrapper->{$field_name}->value();

  // Prepare the shipping address for use in the request.
  if (!empty($order_wrapper->{$field_name}->commerce_customer_address)) {
    $shipping_address = $order_wrapper->{$field_name}->commerce_customer_address->value();
  }
  else {
    $shipping_address = addressfield_default_values();
  }

  $api_vars = commerce_ups_address_validation_decrypt_vars(TRUE);

  $access_request = new SimpleXMLElement('<AccessRequest/>');
  $access_request->addChild('AccessLicenseNumber', $api_vars['ups_accesskey']);
  $access_request->addChild('UserId', $api_vars['ups_userid']);
  $access_request->addChild('Password', $api_vars['ups_password']);

  $address_validation_request = new SimpleXMLElement('<AddressValidationRequest/>');

  $request = $address_validation_request->addChild('Request');
  $transaction_reference = $request->addChild('TransactionReference');
  $transaction_reference->addChild('CustomerContext', 'Address Validation Request');
  $transaction_reference->addChild('XpciVersion', '1.0');
  $request->addChild('RequestAction', 'XAV');
  // TODO: Add different Request Options
  $request->addChild('RequestOption', '3');

  $address = $address_validation_request->addChild('AddressKeyFormat');

  // TODO: Add Building Name
  $address->addChild('AddressLine', $shipping_address['thoroughfare']);
  $address->addChild('PoliticalDivision2', $shipping_address['locality']);
  $address->addChild('PoliticalDivision1', $shipping_address['administrative_area']);
  $address->addChild('PostcodePrimaryLow', $shipping_address['postal_code']);
  $address->addChild('CountryCode', $shipping_address['country']);

  $address_validation_request .= $address_validation_request->asXML();
  $xml = $access_request->asXML() . $address_validation_request;
  return $xml;
}

/**
 * Submits an API request to the Progistics XML Processor.
 *
 * @param $xml
 *   An XML string to submit to the Progistics XML Processor.
 * @param $message
 *   Optional log message to use for logged API requests.
 */
function commerce_ups_address_validation_api_request($xml, $message = '') {
  // Log the API request if specified.
  if (in_array('request', variable_get('commerce_ups_address_validation_log', array()))) {
    if (empty($message)) {
      $message = t('Submitting API request to the UPS Address Validation - Street Level');
    }
    watchdog('ups_address_validation', '@message:<pre>@xml</pre>', array('@message' => $message, '@xml' => $xml));
  }

  // TODO: Add test and production URLs
  // have to tie it into setting on Admin form
  // Test: https://wwwcie.ups.com/ups.app/xml/XAV
  // Production: https://onlinetools.ups.com/ups.app/xml/XAV
  $ch = curl_init('https://onlinetools.ups.com/ups.app/xml/XAV');
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_TIMEOUT, 60);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);

  $result = curl_exec($ch);

  // Log any errors to the watchdog.
  if ($error = curl_error($ch)) {
    watchdog('ups_address_validation', 'cURL error: @error', array('@error' => $error), WATCHDOG_ERROR);
    return FALSE;
  }
  curl_close($ch);

  // If we received data back from the server...
  if (!empty($result)) {
    // Extract the result into an XML response object.
    $response = new SimpleXMLElement($result);

    // Log the API request if specified.
    if (in_array('response', variable_get('commerce_ups_address_validation_log', array()))) {
      watchdog('ups_address_validation', 'API response received:<pre>@xml</pre>', array('@xml' => $response->asXML()));
    }
    return $response;
  }
  else {
    return FALSE;
  }
}

/* This is what the response looks like when it is a valid address

<?xml version="1.0" ?>
<AddressValidationResponse>
    <Response>
        <TransactionReference>
            <CustomerContext>
                Address Validation Request
            </CustomerContext>
            <XpciVersion>
                1.0
            </XpciVersion>
        </TransactionReference>
        <ResponseStatusCode>
            1
        </ResponseStatusCode>
        <ResponseStatusDescription>
            Success
        </ResponseStatusDescription>
    </Response>
    <ValidAddressIndicator/>
    <AddressClassification>
        <Code>
            2
        </Code>
        <Description>
            Residential
        </Description>
    </AddressClassification>
    <AddressKeyFormat>
        <AddressClassification>
            <Code>
                2
            </Code>
            <Description>
                Residential
            </Description>
        </AddressClassification>
        <AddressLine>
            247 E MOHAWK DR
        </AddressLine>
        <Region>
            MALVERN OH 44644-9540
        </Region>
        <PoliticalDivision2>
            MALVERN
        </PoliticalDivision2>
        <PoliticalDivision1>
            OH
        </PoliticalDivision1>
        <PostcodePrimaryLow>
            44644
        </PostcodePrimaryLow>
        <PostcodeExtendedLow>
            9540
        </PostcodeExtendedLow>
        <CountryCode>
            US
        </CountryCode>
    </AddressKeyFormat>
</AddressValidationResponse>

This is what it looks like when it is invalid

<?xml version="1.0" ?>
<AddressValidationResponse>
    <Response>
        <TransactionReference>
            <CustomerContext>
                Address Validation Request
            </CustomerContext>
            <XpciVersion>
                1.0
            </XpciVersion>
        </TransactionReference>
        <ResponseStatusCode>
            1
        </ResponseStatusCode>
        <ResponseStatusDescription>
            Success
        </ResponseStatusDescription>
    </Response>
    <AmbiguousAddressIndicator/>
    <AddressClassification>
        <Code>
            0
        </Code>
        <Description>
            Unknown
        </Description>
    </AddressClassification>
    <AddressKeyFormat>
        <AddressClassification>
            <Code>
                2
            </Code>
            <Description>
                Residential
            </Description>
        </AddressClassification>
        <AddressLine>
            49 NELLS HILL RD
        </AddressLine>
        <Region>
            LIBERTY ME 04949-3255
        </Region>
        <PoliticalDivision2>
            LIBERTY
        </PoliticalDivision2>
        <PoliticalDivision1>
            ME
        </PoliticalDivision1>
        <PostcodePrimaryLow>
            04949
        </PostcodePrimaryLow>
        <PostcodeExtendedLow>
            3255
        </PostcodeExtendedLow>
        <CountryCode>
            US
        </CountryCode>
    </AddressKeyFormat>
</AddressValidationResponse>

*/
