<?php

/**
 * @file
 * Handles Checkout Pane functionality for Commerce Address Validation - Street Level module.
 */

/**
 * Checkout pane callback: returns the settings form elements for the checkout
 * completion message.
 */
function commerce_ups_address_validation_pane_settings_form($checkout_pane) {
  $form = array();
  
  $message = variable_get('commerce_ups_address_validation_message', commerce_ups_address_validation_message_default());

  $form['container'] = array(
    '#type' => 'container',
    '#access' => filter_access(filter_format_load($message['format'])),
  );
  $form['container']['commerce_ups_address_validation_message'] = array(
    '#type' => 'text_format',
    '#title' => t('Address Validation Explanation'),
    '#default_value' => $message['value'],
    '#format' => $message['format'],
  );

  $var_info = array(
    'site' => array(
      'type' => 'site',
      'label' => t('Site information'),
      'description' => t('Site-wide settings and other global information.'),
    ),
    'commerce_order' => array(
      'label' => t('Order'),
      'type' => 'commerce_order',
    ),
  );

  $form['container']['commerce_ups_address_validation_message_help'] = RulesTokenEvaluator::help($var_info);

  return $form;
}

/**
 * Checkout pane callback: builds form if address validation is needed.
 */
function commerce_ups_address_validation_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'commerce_ups_address_validation') . '/includes/commerce_ups_address_validation.checkout_pane.inc';

  $pane_form = array(
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'commerce_ups_address_validation') . '/js/commerce_ups_address_validation.js'),
      'css' => array(drupal_get_path('module', 'commerce_ups_address_validation') . '/css/commerce_ups_address_validation.css'),
    ),
  );

  // Load the address validation message.
  $message = variable_get('commerce_ups_address_validation_message', commerce_ups_address_validation_message_default());

  // Perform translation.
  $message['value'] = commerce_i18n_string('commerce:checkout:addressvalidation:message', $message['value'], array('sanitize' => FALSE));

  // Perform token replacement.
  $message['value'] = token_replace($message['value'], array('commerce-order' => $order), array('clear' => TRUE));

  // Apply the proper text format.
  $message['value'] = check_markup($message['value'], $message['format']);

  $pane_form['message'] = array(
    '#markup' => '<div class="commerce_ups_address_validation-message">' . $message['value'] . '</div>',
  );

  $address_options = commerce_ups_address_validation_process_addresses($order);

  // Substitue message for first address
  $address_options[0] = 'Use address as originally entered.';

  $pane_form['address'] = array(
    '#type' => 'fieldset',
    
  );
  
  $pane_form['address']['selected_address'] = array(
    '#type' => 'radios',
    '#title' => t('Select an address to use'),
    '#options' => $address_options,
    '#required' => TRUE,
  );

  $pane_form['address']['copy_to_billing'] = array(
    '#type' => 'checkbox',
    '#title' => t('Update Billing Address with selected address'),
    '#description' => t('If you check this box the address you entered on the previous page, this will be ignored.'),
    '#default_value' => FALSE,
  );

  return $pane_form;
}

/**
 * Checkout pane callback: submit the address validation details.
 */
function commerce_ups_address_validation_pane_checkout_form_submit($form, &$form_state, $checkout_pane, $order) {
  $inputs = array(
    'selected_address' => $form_state['input']['commerce_ups_address_validation']['address']['selected_address'],
    'copy_to_billing' => $form_state['input']['commerce_ups_address_validation']['address']['copy_to_billing'],
  );
  commerce_ups_address_validation_submit_handler($order, $inputs);
}