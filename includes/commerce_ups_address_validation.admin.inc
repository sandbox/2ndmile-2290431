<?php

/**
 * @file
 * Handles admin settings page for Commerce UPS Address Validation - Street Level module.
 */

/* hook_settings_form() */
function commerce_ups_address_validation_settings_form($form, &$form_state) {
  $ups_link = l(t('UPS.com'), 'https://www.ups.com/upsdeveloperkit', array('attributes' => array('target' => '_blank')));

  $form = array();

  $form['descr']['#markup'] = t('In order to use address validation, you must have an account with UPS. You can apply for UPS API credentials at !ups. If you have the Commerce UPS module installed you can choose to use those credentials below.', array('!ups' => $ups_link));

  if (module_exists('commerce_ups')) {
    $form['commerce_ups_cred'] = array(
      '#type' => 'item',
      '#title' => t('Commerce UPS module API credentials'),
      'status' => array(
        '#type' => 'item',
        '#title' => FALSE,
      ),
    );
  
    $form['commerce_ups_cred']['commerce_ups_address_validation_use_ups_creds'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use Commerce UPS API login credentials'),
      '#description' => t('<strong>Please note: If you do not check this box many of the form fields below are required!</strong>'),
      '#default_value' => variable_get('commerce_ups_address_validation_use_ups_creds', FALSE),
    );
  }

  $form['api'] = array(
    '#type' => 'fieldset',
    '#title' => t('UPS API credentials'),
    '#collapsible' => FALSE,
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'commerce_ups_address_validation') . '/js/commerce_ups_address_validation.js'),
    ),
  );

  $encrypted = variable_get('commerce_ups_address_validation_encrypt', FALSE);
  $api_vars = commerce_ups_address_validation_decrypt_vars(FALSE);

  $form['api']['commerce_ups_address_validation_account_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Account ID'),
    '#default_value' => $api_vars['ups_accountid'],
    '#required' => FALSE,
  );

  $form['api']['commerce_ups_address_validation_user_id'] = array(
    '#type' => 'textfield',
    '#title' => t('User ID'),
    '#default_value' => $api_vars['ups_userid'],
    '#required' => FALSE,
  );

  $form['api']['commerce_ups_address_validation_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#required' => FALSE,
    '#description' => t('Please leave blank if you do not want to update your password at this time.'),
  );

  $form['api']['commerce_ups_address_validation_access_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Access Key'),
    '#default_value' => $api_vars['ups_accesskey'],
    '#required' => FALSE,
  );

  $form['api']['encryption'] = array(
    '#type' => 'item',
    '#title' => t('Encryption'),
    'status' => array(
      '#type' => 'item',
      '#title' => FALSE,
    ),
  );

  if (commerce_ups_address_validation_encryption_available()) {
    $form['api']['encryption']['status']['#markup'] =
      'Encryption is available and configured properly.';
    $form['api']['encryption']['commerce_ups_address_validation_encrypt'] = array(
      '#type' => 'checkbox',
      '#title' => t('Encrypt UPS credentials (HIGHLY RECOMMENDED)'),
      '#description' => t('Note: Enabling this setting automatically will encrypt your password even though you cannot see it. Disabling this checkbox requires that you re-enter a password.'),
      '#default_value' => $encrypted,
    );
  }
  else {
    $aes_link = l(t('AES'), 'http://drupal.org/project/aes', array('attributes' => array('target' => '_blank')));
    $form['api']['encryption']['status']['#markup'] = '<span class="error">' . t('!aes is not installed - your login credentials will not be encrypted.', array('!aes' => $aes_link)) . '</span>';
  }

  if (module_exists('commerce_checkout_progress')) {
    $form['checkout_progress'] = array(
      '#type' => 'item',
      '#title' => t('Commerce Checkout Progress'),
      'status' => array(
        '#type' => 'item',
        '#title' => FALSE,
      ),
    );
  
    $form['checkout_progress']['commerce_ups_address_validation_show_progress_indicator'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show "Address Validation" in Checkout Progress Indicator'),
      '#description' => t('Check the box if you would like the Address Validation step to be included in the Checkout Progree Indicator'),
      '#default_value' => variable_get('commerce_ups_address_validation_show_progress_indicator', FALSE),
    );
  }

  $form['admin_forms'] = array(
    '#type' => 'item',
    '#title' => t('Run Address Validation on Admin Order Create/Edit Forms'),
    'status' => array(
      '#type' => 'item',
      '#title' => FALSE,
    ),
  );

  $form['admin_forms']['commerce_ups_address_validation_execute_admin_forms'] = array(
    '#type' => 'checkbox',
    '#title' => t('Run Address Validation on Admin Order Create/Edit Forms'),
    '#description' => t('Check the box if you would like the Address Validation to run when creating or edit an order on the admin side of your site.'),
    '#default_value' => variable_get('commerce_ups_address_validation_execute_admin_forms', FALSE),
  );

  $form['advanced_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['advanced_options']['commerce_ups_address_validation_log'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Log the following messages for debugging'),
    '#options' => array(
      'request' => t('API request messages'),
      'response' => t('API response messages'),
    ),
    '#default_value' => variable_get('commerce_ups_address_validation_log', array()),
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/*
 * Implements hook_form_validate().
 */
function commerce_ups_address_validation_settings_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  $encrypted = variable_get('commerce_ups_address_validation_encrypt', FALSE) && function_exists('aes_decrypt');
  // If we are not using the UPS Commerce Credentials
  // many of the fields are required
  if (!$values['commerce_ups_address_validation_use_ups_creds']) {
    $fields = array(
      'commerce_ups_address_validation_user_id',
      'commerce_ups_address_validation_account_id',
      'commerce_ups_address_validation_access_key',
    );
    foreach ($fields as $field) {
      if (empty($values[$field])) {
        form_set_error($field, t('This field is required when not using the Commerce UPS API Credentials.'));
      }
    }
  }

  // If the Password field is empty, then they're not trying to update it and we should ignore it.
  if (empty($values['commerce_ups_address_validation_password'])) {
    unset($form_state['values']['commerce_ups_address_validation_password']);
    if ($encrypted && empty($form_state['input']['commerce_ups_address_validation_encrypt'])) {
      form_set_error('commerce_ups_address_validation_password', t('You must enter in a password to turn off encryption.'));
    }
    return;
  }

}

/*
 * Implements hook_form_submit().
 */
function commerce_ups_address_validation_settings_form_submit($form, &$form_state) {
  $encrypted = variable_get('commerce_ups_address_validation_encrypt', FALSE) && function_exists('aes_decrypt');
  // If we are using Commerce UPS credentials
  // get those and save them for our module
  // We do this in case Commerce UPS goes away
  // then we will still have the values
  if (isset($form_state['values']['commerce_ups_address_validation_use_ups_creds']) && $form_state['values']['commerce_ups_address_validation_use_ups_creds']) {
    $use_ups_creds = TRUE;
    $form_state['input']['commerce_ups_address_validation_user_id'] = variable_get('commerce_ups_user_id', '');
    $form_state['input']['commerce_ups_address_validation_account_id'] = variable_get('commerce_ups_account_id', '');
    $form_state['input']['commerce_ups_address_validation_access_key'] = variable_get('commerce_ups_access_key', '');
    $form_state['input']['commerce_ups_address_validation_password'] = variable_get('commerce_ups_password', '');
    $form_state['input']['commerce_ups_address_validation_encrypt'] = variable_get('commerce_ups_encrypt', '');
  } 
  else {
    // we are not using UPS Commerce Credentials
    $use_ups_creds = FALSE;
    // If we want to encrypt, do so
    if (isset($form_state['input']['commerce_ups_address_validation_encrypt']) && $form_state['input']['commerce_ups_address_validation_encrypt']) {
      $form_state['input']['commerce_ups_address_validation_user_id'] = commerce_ups_address_validation_encrypt($form_state['input']['commerce_ups_address_validation_user_id']);
      $form_state['input']['commerce_ups_address_validation_account_id'] = commerce_ups_address_validation_encrypt($form_state['input']['commerce_ups_address_validation_account_id']);
      $form_state['input']['commerce_ups_address_validation_access_key'] = commerce_ups_address_validation_encrypt($form_state['input']['commerce_ups_address_validation_access_key']);
      if (!empty($form_state['input']['commerce_ups_address_validation_password'])) {
        $form_state['input']['commerce_ups_address_validation_password'] = commerce_ups_address_validation_encrypt($form_state['input']['commerce_ups_address_validation_password']);
      }
      elseif ($encrypted == FALSE) {
        // This is an odd case, if encryption is turned on but the password is already in the db
        // Then we need to pull it from the db, not from the field
        $form_state['input']['commerce_ups_address_validation_password'] = commerce_ups_address_validation_encrypt(variable_get('commerce_ups_address_validation_password', ''));
      }
    }
  }

  if (empty($form_state['input']['commerce_ups_address_validation_password'])) {
    unset($form_state['input']['commerce_ups_address_validation_password']);
  }

  if (!isset($form_state['input']['commerce_ups_address_validation_use_ups_creds'])) {
    $form_state['input']['commerce_ups_address_validation_use_ups_creds'] = FALSE;
  }

  if (!isset($form_state['input']['commerce_ups_address_validation_encrypt'])) {
    $form_state['input']['commerce_ups_address_validation_encrypt'] = FALSE;
  }

  if (!isset($form_state['input']['commerce_ups_address_validation_show_progress_indicator'])) {
    $form_state['input']['commerce_ups_address_validation_show_progress_indicator'] = FALSE;
  }

  // Save values from above
  $fields = array(
    'commerce_ups_address_validation_use_ups_creds',
    'commerce_ups_address_validation_account_id',
    'commerce_ups_address_validation_user_id',
    'commerce_ups_address_validation_password',
    'commerce_ups_address_validation_access_key',
    'commerce_ups_address_validation_encrypt',
    'commerce_ups_address_validation_show_progress_indicator',
    'commerce_ups_address_validation_log',
    'commerce_ups_address_validation_execute_admin_forms',
  );

  foreach ($fields as $key) {
    if (array_key_exists($key, $form_state['input'])) {
      $value = $form_state['input'][$key];
      variable_set($key, $value);
    }
  }

  if ($use_ups_creds) {
    drupal_set_message(t('The Commerce UPS Address Validation - Street Level configuration options have been saved using the credentials from the Commerce UPS module.'));
  }
  else {
    drupal_set_message(t('The Commerce UPS Address Validation - Street Level configuration options have been saved.'));
  }
}
