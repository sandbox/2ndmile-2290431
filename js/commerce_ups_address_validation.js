// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.commerce_ups_address_validation = {
  attach: function(context, settings) {
    $(document).ready(function() {
      if ($('#edit-commerce-ups-address-validation-use-ups-creds').is(':checked')) {
        commerce_ups_address_validation_hide(true);
      }
      else {
        commerce_ups_address_validation_hide(false);
      }

      var a = false;
      $('#edit-commerce-ups-address-validation-use-ups-creds').change(function() {
        if ($(this).is(':checked')) { 
          a = true;
        }
        else {
         a = false; 
        }
        commerce_ups_address_validation_hide(a);
      }); 

      function commerce_ups_address_validation_hide(a) {
        if (a) {
          $('#edit-api').add('.form-item-commerce-ups-address-validation-use-ups-creds .description').hide();
        } else {
          $('#edit-api').add('.form-item-commerce-ups-address-validation-use-ups-creds .description').show();
          // add required symbol to form fields
          // when not using Commerce UPS API Credentials
          var $req = '<span class="form-required" title="This field is required.">*</span>';
          $($req).appendTo('.form-item-commerce-ups-address-validation-account-id label');
          $($req).appendTo('.form-item-commerce-ups-address-validation-user-id label');
          $($req).appendTo('.form-item-commerce-ups-address-validation-access-key label');
        }
      }

      // show options on selection
      $('.form-item-commerce-ups-address-validation-address-copy-to-billing').hide();
      $('.form-item-commerce-ups-address-validation-address-selected-address input').change(function() {
        if ($(this).val() == 0) {
          // if customer selects original address
          // display warning message
          $('#edit-commerce-ups-address-validation-address-selected-address-0').parent().find('label').append(' <span class="validation-warning">You accept responsibility for any incorrectly delivered packages.</span>');
          $('.form-item-commerce-ups-address-validation-address-copy-to-billing').hide();
        } else if ($(this).val() != 0) {
          $('.form-item-commerce-ups-address-validation-address-copy-to-billing').show();
          $('.form-item-commerce-ups-address-validation-address-copy-to-billing .description').hide();
          $('.validation-warning').remove();
        }
      });

      // show options on selection
      $('.form-item-copy-to-billing').hide();
      $('.form-item-selected-address input').change(function() {
        if ($(this).val() != 0) {
          $('.form-item-copy-to-billing').show();
          $('.form-item-copy-to-billing .description').hide();
        } else {
          $('.form-item-copy-to-billing').hide();
        }
      });

    });
  }
};

})(jQuery, Drupal, this, this.document);
